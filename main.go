package main

import (
	"github.com/PuerkitoBio/goquery"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"github.com/gorilla/mux"
	"github.com/jasonlvhit/gocron"
	"log"
	"net/http"
	"encoding/json"
	"io"
	"time"
	"io/ioutil"
	"fmt"
)

type Comment struct {
	Id string `json:"id"`
	Text string `json:"text"`
	Date time.Time `json:"date"`
}

type DateInterval struct {
	Start time.Time `json:"start"`
	End time.Time `json:"end"`
}

type CommentsCollection []Comment

func main() {
	gocron.Every(1).Hour().Do(writeInDB)

	router := mux.NewRouter()
	router.HandleFunc("/", List)
	log.Fatal(http.ListenAndServe(":8080", router))
}

func parse() CommentsCollection {
	var collection CommentsCollection

 	doc, _ := goquery.NewDocument(Link)
	doc.Find("figure").Each(func(_ int, selection *goquery.Selection) {
		content := selection.Find("div > blockquote").Text()
		dateString, _ := selection.Find("div > figcaption > span[class=\"review-datepublished\"] > time").Attr("datetime")
		id, _ := selection.Find("div > figcaption > div[class=\"review-helpful-rating\"]").Attr("data-review-id")

		date, _ := time.Parse(time.RFC3339, dateString)
		collection = append(collection, Comment{id, content, date})
	})

	return collection
}

func writeInDB() {
	session, err := mgo.Dial(MongoHost)

	if err!= nil {
		panic(err)
	}

	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	for _, comment := range parse() {
		result, _ := session.DB("parser").C("comments").Find(bson.M{"id": comment.Id}).Count()
		if result == 0 {
			session.DB("parser").C("comments").Insert(comment)
		}
	}
}

func List(response http.ResponseWriter, request *http.Request)  {
	var results CommentsCollection
	response.Header().Set("Content-type", "application/json; charset=UTF-8")
	response.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")
	response.Header().Set("Access-Control-Request-Method", "GET, POST")
	response.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	response.WriteHeader(http.StatusOK)


	body, err := ioutil.ReadAll(io.LimitReader(request.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := request.Body.Close(); err != nil {
		panic(err)
	}

	var dateInterval DateInterval

	if err := json.Unmarshal(body, &dateInterval); err != nil {
		if err := json.NewEncoder(response).Encode(err); err != nil {
			panic(err)
		}
	}

	session, err := mgo.Dial(MongoHost)

	if err!= nil {
		panic(err)
	}

	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	var emptyTime time.Time
	var err2 error
	if dateInterval.Start != emptyTime && dateInterval.End != emptyTime {
		err2 = session.DB("parser").C("comments").Find(bson.M{"date": bson.M{"$gte": dateInterval.Start, "$lte": dateInterval.End}}).All(&results)
	} else {
		err2 = session.DB("parser").C("comments").Find(nil).All(&results)
	}

	if err2 != nil {
		panic(err2)
	}

	jsonResponse, err := json.Marshal(results)
	if err != nil {
		fmt.Println(err)
	}

	io.WriteString(response, string(jsonResponse))
}
